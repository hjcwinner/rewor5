const mongoose = require('mongoose')
const orderModel = mongoose.Schema({
    product : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'rproduct',
        required : true
    },
    quantity : {
        type : Number,
        required : true
    }
})

module.exports = mongoose.model('order', orderModel)