const mongoose = require('mongoose');

const db_Option = {
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify : false,
    useCreateIndex: true
}

mongoose
    .connect(process.env.MONGODB_URI, db_Option)
    .then(() => console.log("연결완료"))
    .catch(err => console.log(err.message));