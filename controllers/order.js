const orderModel = require('../model/order')

exports.orders_get_all = (req, res) => {
    orderModel
    .find()
    .populate('product',["pname","pprice"])
    .then(docs => {
        const response = {
            count : docs.length,
            products : docs.map(doc =>{
                return{
                    id : doc._id,
                    product : doc.product,
                    quantity : doc.quantity,
                    request : {
                        type : "get",
                        url : "http://localhost:9090/order/" + doc._id
                    }
                }
            })
        }
       res.json(response) 
    })
    .catch(err => {
        res.json({
            message : err.message
        })
    })
}

exports.orders_get_detail = (req, res) => {
    orderModel
        .findById(req.params.orderid)
        .populate('product',["pname","pprice"])
        .then(doc => {
            res.json({
                id : doc._id,
                product : doc.product,
                quantity : doc.quantity,
                request : {
                    type : "get",
                    url : "http://localhost:9090/order"
                }

            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_post_product = (req, res) => {
    const order = new orderModel({
        product : req.body.orproduct,
        quantity : req.body.orqty
    })

    order
        .save()
        .then(doc => {
            res.json({
                message : "save order",
                id : doc._id,
                product : doc.product,
                quantity : doc.quantity
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_patch_product = (req, res) => {
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propNmae] = ops.value
    }

    orderModel
        .findByIdAndUpdate(req.params.orderid, { $set : updateOps})
        .then(doc => {
            res.json({
                message : "update complete",
                request : {
                    type : "get",
                    url : "http://localhost:9090/order/" + doc._id
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_delete_product = (req, res) => {
    orderModel
        .findByIdAndDelete(req.params.orderid)
        .then(() => {
            res.json({
                message : "delete",
                request : {
                    type : "GET",
                    url : "http://localhost:9090/order"
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}