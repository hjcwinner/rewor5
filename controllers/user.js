const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const userModel = require('../model/user')
const user = require('../model/user')

exports.userregister = (req, res) => {
    userModel
        .findOne({email : req.body.email})
        .then(user => {
            if(user) {
                return res.json({
                    message : "이미가입된이메일"
                })
            }
            else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if(err) 
                    {
                        return res.json({
                            message : err.message
                        })
                    }
                    else 
                    { 
                        const user = new userModel({
                            username : req.body.username,
                            email : req.body.email,
                            password : hash
                        })
                    
                        user
                            .save()
                            .then(result => {
                               res.json({
                                  userInfo : {
                                    id : result._id,
                                    username : result.username,
                                    email : result.email,
                                    password : result.password
                                  }
                               }) 
                            })
                            .catch(err => {
                                res.json({
                                    message : err.message
                                })
                            })
                    }
                })
            }

        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.userlogin = (req, res) => {
    ////email중복체크 => password확인 => login
        userModel
            .findOne({email : req.body.email})
            .then(user => {
                if(!user) {
                    return res.json({
                        message : "가입된 email이 아닙니다"
                    })
                }
                else {
                    bcrypt.compare(req.body.password, user.password, (err, isMatch) => {
                        if(err || isMatch === false) {
                            return res.json({
                                message : "password incorrect"
                            })
                        }
                        else {
                            const token = jwt.sign(
                                {proid : user._id},
                                process.env.SECRET_KEY,
                                { expiresIn : 60 }
                            )
                            res.json({
                                message : "successful login",
                                token : token
                            })
                        }
                    })
                }
            })
            .catch(err => {
                res.json({
                    message : err.message
                })
            })
    }