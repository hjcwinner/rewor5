const productModel = require('../model/product')

exports.products_get_all = (req, res) => {
    productModel
        .find()
        .then(docs => {
            res.json({
                count : docs.length,
                message : "all product",
                allinfo : docs.map(doc => {
                    return {
                        id : doc._id,
                        name : doc.pname,
                        price : doc.pprice,
                        request : {
                            type : "get",
                            url : "http://localhost:9090/product/" + doc._id
                        }
                    }        
                })
            })        
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.products_get_detail = (req, res) => {
    productModel
        .findById(req.params.ttttaaa)
        .then(doc => {
            if(!doc)
            {
                res.json({
                    message : "그런제품없음"
                })
            }
            else
            {
                res.json({
                    message : "1product   " + req.params.ttttaaa,
                    productInfo : {
                        id : doc._id,
                        name : doc.pname,
                        price : doc.pprice,
                        request : {
                            type : "get",
                            url : "http://localhost:9090/product"
                        }
                    }
                })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.products_post_product = (req, res) => {
    const prodd = new productModel({
        pname : req.body.papaname,      
        pprice : req.body.papaprice
    })

    prodd
        .save()
        .then(allpri => {
            res.json({
                message : "save product",
                infomation : {
                    id : allpri._id,
                    name : allpri.pname,
                    price : allpri.pprice
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.products_patch_product = (req, res) => {
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }

    productModel
        .findByIdAndUpdate(req.params.productid, { $set : updateOps})
        .then(result => {
            res.json({
                message : "update",
                productInfo : result
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        }) 
}

exports.products_delete_product = (req, res) => {
    productModel
       .findByIdAndDelete(req.params.popopo)
       .then(() => {
           res.json({
               message : "삭제완료"
           })
       })
       .catch(err => {
           res.json({
               message : err.message
           })
       })
}


