const express = require('express')
const bodyParser = require('body-parser') ////설치후에 상수화 시켜준다
const morgan = require('morgan')
const dotenv = require('dotenv')
dotenv.config()

const app = express()
const productRoutes = require('./routes/product')
const orderRoutes = require('./routes/order')
const userRoutes = require('./routes/user')

////database
require("./config/database")


//////middlewere (미들웨어 프론트와 백엔드 사이에 연결부위)
app.use(bodyParser.json())     /////이건 뭐 그렇다는 느낌으로
app.use(bodyParser.urlencoded({extended:false}))  /////이건 뭐 그렇다는 느낌으로
app.use(morgan("dev"))


app.use('/product', productRoutes)
app.use('/order', orderRoutes)
app.use('/user', userRoutes)

app.listen(process.env.PORT, console.log("서버가 시작되다"))
