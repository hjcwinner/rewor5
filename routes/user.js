const express = require('express')
const { userregister, userlogin } = require('../controllers/user')
const router = express.Router()



////register
////email중복체크 => password암호화 => 회원가입
router.post('/register', userregister)

////login
router.post('/login', userlogin)

module.exports = router