const express = require('express')
const { orders_get_all, orders_get_detail, orders_post_product, orders_patch_product, orders_delete_product } = require('../controllers/order')
const router = express.Router()



////전체보기
router.get('/', orders_get_all)

////1개보기
router.get('/:orderid', orders_get_detail)

////등록하기
router.post('/', orders_post_product)

////수정하기
router.patch('/:orderid', orders_patch_product)

////삭제하기
router.delete('/:orderid', orders_delete_product)

module.exports = router