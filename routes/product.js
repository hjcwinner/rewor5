const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth');
const { products_get_all, products_post_product, products_patch_product, products_delete_product, products_get_detail} = require('../controllers/product');


//// 불러오기
router.get("/", checkAuth, products_get_all)


//// 제품1개 불러오기
router.get("/:ttttaaa", products_get_detail)


////등록하기
router.post("/", products_post_product)


/////수정하기
router.patch("/:productid", products_patch_product)


/////삭제하기
router.delete("/:popopo", products_delete_product)

module.exports = router